# Guros Test
This is an application which provides a webservice to decide if a DNA chain is mutated or not.
This is a dockerized project, the application can be executed into a docker container or if you prefer, you can install the Laravel application and the database locally. The source code and database are included in this repo.


## Installation(The short way)
The application contains all the necesary code to be dockerized in order to agilize the testing project. That means that you don't need to install an http server or database server to run the application, that makes easier all the test you need. You only need [docker](https://docs.docker.com/get-docker/) installed to set it up.  

1.- Download the image from docker hub
```bash
sudo git clone https://gitlab.com/alsalazar13/guros_dna.git
```

Note: In this case we push all the laravel app content to the repo in order to agilize the test process, usually the folder vendor and file .env souldn't be pushed.

2.- Rename the file .env.example as .env
```bash
mv .env.example .env
```
3.- Edit the next parameters into the .env file
```bash
APP_NAME=guros
APP_URL=http://localhost:8000
DB_CONNECTION=mysql
DB_HOST=db
DB_PORT=3306
DB_DATABASE=guros
DB_USERNAME=guros_user
DB_PASSWORD=Freddylsca
```

4.- Build the app
```bash
docker-compose build --no-cache app
```

5.- Re-Build, create, start and attaches to containers for the guros service
```bash
docker-compose up -d
```

6.-Install and update all the necesary packages to let the application run correctly
```bash
docker-compose exec app composer update
```

7.-Generate the key for the application
```bash
docker-compose exec app php artisan key:generate
```

## Installation(The long way)
If you decide not use docker to test the application, you will need to execute it locally. For this purpuse, you need to import the database contained into the file database.sql.

In the other hand you will need to set up your laravel application manually. For more information see the official laravel documentation for the right installation [here](https://laravel.com/docs/8.x/installation). Also you need to make sure of testing the webservices provides by the application using your custom routes configured into the app(If you decided to change the current configured routes). 

To install locally you need to follow the next steps:\
1.- Download the repo content into the public folder of your web server\
2.- Install and update dependencies
```bash
sudo composer update
```
3.- Set permissions to application\
It's important to emphasize the fact that the process to set up a laravel configuration could change depending of the operating system where the application will be installed. This process is for an Ubuntu based server. 
```bash
cd guros_dna
sudo chmod -R 777 path/to/root
sudo chown -R www-data:www-data /path/to/guros_dna
sudo usermod -a -G www-data ubuntu
sudo find /path/to/guros_dna -type f -exec chmod 644 {} \;
sudo find /path/to/guros_dna -type d -exec chmod 755 {} \;
sudo chgrp -R www-data storage bootstrap/cache
sudo chmod -R ug+rwx storage bootstrap/cache
```
4.- Create a database called **guros**(This could change if you prefer)\
5.- Create a new mysal user callex **guros_user**(This could change if you prefer)\
6.- Grant permission to the user created over the **guros** database\
7.- Import the content of the file **database.sql** located on the root folder of this application to the database **guros**\
8.- Rename the file .env.example as .env
```bash
mv .env.example .env
```

9.- Edit the file .env located on the root application folder, with the next values\
```php
DB_CONNECTION=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=guros
DB_USERNAME=guros_user
DB_PASSWORD=PASSWORD
```
10.- Edit the main database configuration on the file config/database.php\
```php
'main' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'guros'),
            'username' => env('DB_USERNAME', 'guros_user'),
            'password' => env('DB_PASSWORD', 'PASSWORD'),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
]     
```

11.- Test the application, the routes defined by default are\
http://localhost/guros_dna/public/api/stats \
http://localhost/guros_dna/public/api/mutation
   

# Usage
For the test, we have included the Postman collection, you can find it into the root directory with the name **guros_dna.postman_collection.json**, you just need to import it and run the request, this collection includes:
- Endpoint /mutation (Docker)
- Endpoint /stats (Docker)
- Endpoint /mutation (AWS)
- Endpoint /stats (AWS)

Endpoints for totally local installation was not included because the routes could change depending on your own requierements. 

## /mutation 
- URL (Docker) \
http://localhost:8000/api/mutation 
\
\
To test this endpoint, you need to follow all the steps mentioned in section **Installation(The short way)**. Import the Postman collection included in this application to finally, run de request  **Test DNA LOCAL**.


- URL (AWS) \
http://ec2-13-59-49-129.us-east-2.compute.amazonaws.com/api/mutation 
\
\
To test this endpoint, you just need to import the Postman collection included in this application, and run the request **Test DNA AWS**. That's because the endpoint is on the cloud and it's ready to use.

- Method \
POST

- Parameters \
Array with the DNA sample to check.
```javascript
{
	"dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
}
```

- Success Response(If the DNA sample has a mutation):
	- **Code:** 200
	- **Content:** true


- Error Response(If the DNA sample hansn't a mutation):
	- **Code:** 403
	- **Content:** false

- Success Response(If the database is empty):
	- **Code:** 200
	- **Content:** {"msg":"Database empty"}

- Error Response(If the DNA sample contains non valid characters, only A, T, C, G are accepted. Or the Matrix is not in NxN format):
	- **Code:** 403
	- **Content:** INVALID DATA


- Error Response(If the body data is not in the right format):
	- **Code:** 403
	- **Content:** INVALID BODY DATA FORMAT, JSON REQUIRED


**Important** \
If the DNA sample has a mutation a new record will be added to the database. Checking database additions depends on the method you are running the appplication:
	
- Docker(Local Execution):
```bash
docker exec -it guros-db bash 
```
At this point you will be into the container of the database and you'll be able to connect to MYSQL promt.
```bash
mysql -u guros_user -p
```

The predefined password for this mysql instance is **Freddylsca**. \
The used databased is **guros**. \
The used table is **dna**.

- Local Laravel install:
For this mode, you need to connect to the Mysql server where you have dumped the database included in the application. 

## /stats 
- URL (Docker) \
http://localhost:8000/api/stats
\
\
To test this endpoint, you need to follow all the steps mentioned in section **Installation(The short way)**. Import the Postman collection included in this application to finally, run de request  **Get STATS LOCAL**.

- URL (AWS)
\
\
http://ec2-13-59-49-129.us-east-2.compute.amazonaws.com/api/stats  
To test this endpoint, you just need to import the Postman collection included in this application and run the request **Get STATS AWS**. That's because the endpoint is on the cloud and it's ready to use.

- Method \
GET

- Success Response:
	- **Code:** 200
	- **Content(Example):** 
      ```javascript
      {
        "count_no_mutations":1,
        "count_mutations":4,
        "ratio":80
      }
      ```
		
## License
[MIT](https://choosealicense.com/licenses/mit/)