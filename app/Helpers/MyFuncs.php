<?php

namespace App\Helpers;
use Config;
use DB;
use App\Models\dnaModel;

class MyFuncs {
    public static function lookForMutations($originalMatrix){
        $response=false;

        foreach ($originalMatrix as $keyOriginalMatrix => $valueOriginalMatrix) {
            if(preg_match('/(\w)\1{3,}/', $valueOriginalMatrix)){
                $response=true;
            }
        }

        $aryReversedMatrix=array();
        foreach ($originalMatrix as $keyHorizontal => $valueHorizontal) {
              $currentRow=str_split($valueHorizontal);
              foreach ($currentRow as $key_currentRow => $value_currentRow) {
                  $aryReversedMatrix[$key_currentRow][$keyHorizontal]=$value_currentRow;
              }
        }

        foreach ($aryReversedMatrix as $keyReversedMatrix => $valueReversedMatrix) {
            $stringToEvaluate=implode($valueReversedMatrix);
            if(preg_match('/(\w)\1{3,}/', $stringToEvaluate)){
                $response=true;
            }   
        }

        return $response;

    }


    public static function storestats($matrix,$result){
        $response=true;
        $dnaModel=new dnaModel();
        $rawChain=implode($matrix);
        $encodedChain=base64_encode($rawChain);

        $count=$dnaModel->check_if_exists($encodedChain);
        
        if($count==0){
            $response=$dnaModel->store_chain($encodedChain,$result);
        }
    }

}   
