<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use lluminate\Http\Response;
use App\Helpers\MyFuncs;
use App\Models\dnaModel;
use Config;


class adn extends BaseController
{
    
    public function hasMutation(Request $request){
    	Config::set('database.default', "main");
        $matrix=$request->dna;

        $evaluateDnaSample=MyFuncs::lookForMutations($matrix);

        if($evaluateDnaSample){
            MyFuncs::storestats($matrix,true);
            return response(true, 200)
                  ->header('Content-Type', 'text/plain');
        }else{
            MyFuncs::storestats($matrix,false);
            return response(false, 403)
                  ->header('Content-Type', 'text/plain');
        }

    }

    public function stats(Request $request){
        Config::set('database.default', "main");
        $dnaModel=new dnaModel();

        $aryResponse=array();
        $statsData=$dnaModel->get_stats();

        $total=0;

        foreach ($statsData as $key => $value_stats) {
            $total+=$value_stats->qty;

            if($value_stats->mutation=="Y"){
                $aryResponse['count_mutations']=$value_stats->qty;
            }else{
                $aryResponse['count_no_mutations']=$value_stats->qty;
            }
        }

        if($total==0){
            $aryResponse['msg']="Database empty";            
        }elseif(!isset($aryResponse['count_mutations'])){
            $aryResponse['ratio']=0;
        }else{
            $aryResponse['ratio']=round($aryResponse['count_mutations']/$total , 2);
        }
        
        return response(json_encode($aryResponse), 200)
                  ->header('Content-Type', 'text/plain');

    }
}
