<?php

namespace App\Http\Middleware;
use Closure;

class dnaChainValidator{
    public function handle($request, Closure $next){
        if(is_array($request->dna) && isset($request->dna)){

                $res=true;
                $matrix=$request->dna;
                $len_first_element=strlen($matrix[0]);
                $str_len=0;


                foreach ($matrix as $key => $value) {
                    $str_len=strlen($value);
                    if($str_len!=$len_first_element){
                        $res=false;
                    }
                }


                foreach ($matrix as $key => $value) {
                    $strlen=strlen($value);

                    $coincidences  = preg_match_all('/[ATCG]/i',$value,$matches);

                    if($coincidences!=$strlen){
                        $res=false;
                    }
                }

                if(!$res){
                    abort(403, "Invalid data");
                }else{
                    $response = $next($request);
                    return $response;
                }
        }else{
            abort(403, "Invalid body data format, JSON required");
        }
    }
}