<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;

class dnaModel extends Model{
    protected $table='dna';
    protected $primaryKey = 'id_dna';
    protected $fillable = [];

    public $timestamps = false;

    protected $connection;
    
    public function __construct(){
        $this->connection = Session::get('connection');
    }

    public function check_if_exists($chain){
        $result=$this->where('chain', $chain)->count();

        return $result;
    }

    public function store_chain($chain,$result){
        $mutation=($result)?"Y":"N";
        $result=$this->insert([
            'chain' => $chain,
            'mutation' => $mutation,
            'date'=>DB::raw('CURDATE()'),
            'time'=>DB::raw('CURTIME()')
        ]);

        return $result;
    }

    public function get_stats(){
        $result=$this->select('mutation',DB::raw('COUNT(*) AS qty'))
                  ->groupBy('mutation')
                  ->get();
        return $result;
    }

}



