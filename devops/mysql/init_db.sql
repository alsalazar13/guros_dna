DROP TABLE IF EXISTS `dna`;
CREATE TABLE `dna` (
  `id_dna` int NOT NULL AUTO_INCREMENT,
  `chain` varchar(500) DEFAULT NULL,
  `mutation` varchar(1) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  PRIMARY KEY (`id_dna`)
) ENGINE=InnoDB AUTO_INCREMENT=1;
